const Calendar = props => {
	const { date } = props;
	const days = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
	const months = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
	const weekDay = days[date.getDay()];
	const day = date.getDate();
	const month = months[date.getMonth()];
	const year = date.getFullYear();
	const monthFull = date.toLocaleString('ru', { month: 'long' });
	const monthFullResult = monthFull[0].toUpperCase() + monthFull.slice(1);	

	Date.prototype.daysInMonth = function () {
		return 32 - new Date(this.getFullYear(), this.getMonth(), 32).getDate();
	};
	const daysCounts = date.daysInMonth();

	function getWeeks(year, month) {
		const l = new Date(year, month + 1, 0);
		return Math.ceil((l.getDate() - (l.getDay() ? l.getDay() : 7)) / 7) + 1;
	}
	const weekCounts = getWeeks(year, date.getMonth()); 
	const rowsArray = [];
	rowsArray.length = weekCounts;

	console.log(rowsArray);
	
	

	return (
		<div className="ui-datepicker">
			<div className="ui-datepicker-material-header">
				<div className="ui-datepicker-material-day">{weekDay}</div>
				<div className="ui-datepicker-material-date">
					<div className="ui-datepicker-material-day-num">{day}</div>
					<div className="ui-datepicker-material-month">{month}</div>
					<div className="ui-datepicker-material-year">{year}</div>
				</div>
		  	</div>
		  	<div className="ui-datepicker-header">
		        <div className="ui-datepicker-title">
		        	<span className="ui-datepicker-month">{monthFullResult}</span>&nbsp;<span class="ui-datepicker-year">{year}</span>
				</div>
			</div>
			<table className="ui-datepicker-calendar">
				<colgroup>
					<col />
					<col />
					<col />
					<col />
					<col />
					<col className="ui-datepicker-week-end" />
					<col className="ui-datepicker-week-end" />
			    </colgroup>
			    <thead>
					<tr>
						<th scope="col" title="Понедельник">Пн</th>
						<th scope="col" title="Вторник">Вт</th>
						<th scope="col" title="Среда">Ср</th>
						<th scope="col" title="Четверг">Чт</th>
						<th scope="col" title="Пятница">Пт</th>
						<th scope="col" title="Суббота">Сб</th>
						<th scope="col" title="Воскресенье">Вс</th>
					</tr>
				</thead>
				<tbody>
					{rowsArray}
				</tbody>
			</table>
		</div>
	);
}; 
'use strict';

const Listing = ({ items }) => {    
    const list = items.map(item => {
        const titleOutput = title => { 
            if (title.length > 50) {    
                const shorttitle = title.slice(0, 50);          
                return `${shorttitle}...`;
            } else {
                return title;
            }
        };
        const resultPrice = (code, price) => {
            if (code === 'USD') {
                return `$${price}`;
            } else if (code === 'EUR') {
                return `€${price}`;
            } else {
                return `${price} ${code}`;
            }
        };
        const resultCount = quantity => {
            if (quantity <= 10) {
                return 'item-quantity level-low';
            } else if (quantity <= 20) {
                return 'item-quantity level-medium';
            } else if (quantity > 20) {
                return 'item-quantity level-high';
            }
        };

        return (
            <div className="item" key={item.listing_id}>
                <div className="item-image">
                    <a href={item.url}>
                        <img src={item.MainImage.url_570xN} />
                    </a>
                </div>
                <div className="item-details">
                    <p className="item-title">{titleOutput(item.title)}</p>
                    <p className="item-price">{resultPrice(item.currency_code, item.price)}</p>
                    <p className={resultCount(item.quantity)}>{item.quantity} left</p>
                </div>
            </div>
        );
    });
    
    return (
        <div className="item-list">
            {list}
        </div>
    );
};

Listing.defaultProps = {
    items: []
};

const xhr = new XMLHttpRequest();

xhr.open(
    'GET',
    'https://neto-api.herokuapp.com/etsy',
    false
);

xhr.send();

const data = JSON.parse(xhr.responseText);

ReactDOM.render(
    <Listing items={data} />,
    document.getElementById('root')
);
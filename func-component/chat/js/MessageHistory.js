'use strict';

const MessageHistory = ({ list }) => {
    let messagesList = list.map(item => {
        if (item.type === 'message') {
            return <Message key={item.id} from={item.from} message={item} />;
        } 
        if (item.type === 'response') {
            return <Response key={item.id} from={item.from} message={item} />;
        }
        if (item.type === 'typing') {
            return <Typing key={item.id} from={item.from} message={item} />;
        }
    });  
    
    if (list.length > 0) {
        return (
            <ul>{messagesList}</ul>
        );
    } else {
        return null;
    }
} 

MessageHistory.defaultProps = {
    list: []
};
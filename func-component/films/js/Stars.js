'use strict';

function Stars({count}) {
    let tempArr = [];
    for (let i = 1; i <= count; i++) {
      tempArr.push(i);
    }
    const items = tempArr.map(() => <li><Star /></li>);
    if (items.length > 0 && items.length < 6) {
        return <ul className="card-body-stars u-clearfix">{items}</ul>;
    } else {
        return null;
    }
}

Stars.defaultProps = {
  count: 0
};
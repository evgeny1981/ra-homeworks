'use strict';

const AuthForm = ({ onAuth }) => {
    const user = {};
    const getName = event => {
        user.name = event.currentTarget.value;        
    };
    const getEmail = event => {
        user.email = event.currentTarget.value;
    };
    const getPassword = event => {
        user.password = event.currentTarget.value;
    };

    const formSubmit = event => {
        event.preventDefault();
        onAuth(user);
    };
    
    return (
        <form className="ModalForm" action="/404/auth/" method="POST">
            <div className="Input">
                <input onChange={getName} required type="text" placeholder="Имя" />
                <label></label>
            </div>
            <div className="Input">
                <input onChange={getEmail} type="email" placeholder="Электронная почта" />
                <label></label>
            </div>
            <div className="Input">
                <input onChange={getPassword} required type="password" placeholder="Пароль" />
                <label></label>
            </div>
            <button onClick={formSubmit} type="submit">
                <span>Войти</span>
                <i className="fa fa-fw fa-chevron-right"></i>
            </button>
        </form>
    );
};
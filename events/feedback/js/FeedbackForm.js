'use strict';

const FeedbackForm = ({ data, onSubmit }) => {
    let nameField, emailField, subjectField, messageField, form;

    const salutation = data => {  

        const defaultActive = value => {
            if (data === value) {
                return true;
            } else {
                return false;
            }
        }; 

        return (
            <div className="contact-form__input-group">
                <input defaultChecked={defaultActive('Мистер')} className="contact-form__input contact-form__input--radio" id="salutation-mr" name="salutation" type="radio" value="Мистер" />
                <label className="contact-form__label contact-form__label--radio" htmlFor="salutation-mr">Мистер</label>
                <input defaultChecked={defaultActive('Мисис')} className="contact-form__input contact-form__input--radio" id="salutation-mrs" name="salutation" type="radio" value="Мисис" />
                <label className="contact-form__label contact-form__label--radio" htmlFor="salutation-mrs">Мисис</label>
                <input defaultChecked={defaultActive('Мис')} className="contact-form__input contact-form__input--radio" id="salutation-ms" name="salutation" type="radio" value="Мис" />
                <label className="contact-form__label contact-form__label--radio" htmlFor="salutation-ms">Мис</label>
            </div>
        );
    };

    const name = (
        <div className="contact-form__input-group">
            <label className="contact-form__label" htmlFor="name">Имя</label>
            <input ref={el => nameField = el} defaultValue={data.name} className="contact-form__input contact-form__input--text" id="name" name="name" type="text" />
        </div>
    );

    const email = (
        <div className="contact-form__input-group">
            <label className="contact-form__label" htmlFor="email">Адрес электронной почты</label>
            <input ref={el => emailField = el} defaultValue={data.email} className="contact-form__input contact-form__input--email" id="email" name="email" type="email" />
        </div>
    );

    const subject = (
        <div className="contact-form__input-group">
            <label className="contact-form__label" htmlFor="subject">Чем мы можем помочь?</label>
            <select ref={el => subjectField = el} defaultValue={data.subject} className="contact-form__input contact-form__input--select" id="subject" name="subject">
                <option>У меня проблема</option>
                <option>У меня важный вопрос</option>
            </select>
        </div>
    );

    const message = (
        <div className="contact-form__input-group">
            <label className="contact-form__label" htmlFor="message">Ваше сообщение</label>
            <textarea ref={el => messageField = el} defaultValue={data.message} className="contact-form__input contact-form__input--textarea" id="message" name="message" rows="6" cols="65"></textarea>
        </div>
    );

    const snacks = data => {       

        const defaultActive = value => {
            let result = false;            
            if (data !== undefined) {
                data.forEach(el => {
                    if (el === value) {
                        result = true;
                    }
                });
            } 
            return result;
        };
        
        return (
            <div className="contact-form__input-group">
                <p className="contact-form__label--checkbox-group">Хочу получить:</p>
                <input defaultChecked={defaultActive('пицца')} className="contact-form__input contact-form__input--checkbox" id="snacks-pizza" name="snacks" type="checkbox" value="пицца" />
                <label className="contact-form__label contact-form__label--checkbox" htmlFor="snacks-pizza">Пиццу</label>
                <input defaultChecked={defaultActive('пирог')} className="contact-form__input contact-form__input--checkbox" id="snacks-cake" name="snacks" type="checkbox" value="пирог" />
                <label className="contact-form__label contact-form__label--checkbox" htmlFor="snacks-cake">Пирог</label>
            </div>
        );
    };

    const dataForm = event => {
        event.preventDefault();
        const dataObj = {};
        const salutationField = form.querySelector('input[type=radio]:checked');
        const snacksField = form.querySelector('input[type=checkbox]:checked');
        dataObj.salutation = salutationField.value;
        dataObj.name = nameField.value;
        dataObj.subject = subjectField.value;
        dataObj.message = messageField.value;
        dataObj.email = emailField.value;
        dataObj.snacks = snacksField.value;       
        onSubmit(JSON.stringify(dataObj));
    };



    return (
        <form ref={el => form = el} className="content__form contact-form">
            <div className="testing">
                <p>Чем мы можем помочь?</p>
            </div>
            {salutation(data.salutation)}
            {name}
            {email}
            {subject}
            {message}
            {snacks(data.snacks)}
            <button className="contact-form__button" onClick={dataForm} type="submit">Отправить сообщение!</button>
            <output id="result" />
        </form>
    );
};
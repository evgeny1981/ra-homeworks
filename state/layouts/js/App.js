'use strict';

const VIEW_LIST = "view_list";
const VIEW_MODULE = "view_module";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      view: VIEW_MODULE
    }
  }

  switcherView = (view) => {
    if (view === VIEW_LIST) {
      this.setState({
        view: VIEW_MODULE
      });
    } else if (view === VIEW_MODULE) {
      this.setState({
        view: VIEW_LIST
      });
    }
  };

  renderBool = (view) => {
    if (view === VIEW_MODULE) {
      return true;
    } 
    return false;
  };

  render() {
    return (
      <div>
        <div className="toolbar">
          <IconSwitch
            icon={this.state.view}
            onSwitch={() => { this.switcherView(this.state.view) }}
          />
        </div>
        {this.renderLayout(this.renderBool(this.state.view))}
      </div>
    );
  }

  renderLayout(cardView) {
    if (cardView) {
      return (
        <CardsView
          layout={this.props.layout}
          cards={this.getShopItems(this.props.products, cardView)} />
      );
    }
    return (<ListView items={this.getShopItems(this.props.products, cardView)} />);
  }

  getShopItems(products, cardView) {
    return products.map(product => {
      let cardProps = {
        title: product.name,
        caption: product.color,
        img: product.img,
        price: `$${product.price}`
      };
      if (cardView) {
        return (
          <ShopCard {...cardProps}/>
        );
      }
      return (<ShopItem {...cardProps}/>)
    });
  }
}

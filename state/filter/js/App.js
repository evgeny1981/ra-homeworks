'use strict'

class App extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          selected: 'All'
      }
  }

    selectFilter = (filter) => {
        this.setState ({
            selected: filter
        });        
    };

    selectContent = (list) => {
        if (this.state.selected === 'All') {
            return list;
        } else {
            return list.filter(el => el.category === this.state.selected)
        }
    };
  

    render() {        
        return(
            <div>
                <Toolbar
                    filters={this.props.filters}
                    selected={this.state.selected}
                    onSelectFilter={(filter) => {this.selectFilter(filter)}}
                />
                <Portfolio projects={this.selectContent(this.props.projects)} />
            </div>
        );
    }
}

